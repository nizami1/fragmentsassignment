package com.example.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.navigation.Navigation


class fragmentOne : Fragment(R.layout.fragment_one) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val editTextInput = view.findViewById<EditText>(R.id.inputtext)
        val sendButton = view.findViewById<Button>(R.id.buttonSend)
        val controller = Navigation.findNavController(view)
        sendButton.setOnClickListener {

            val inputText = editTextInput.toString()

            if(inputText.isEmpty()){
                return@setOnClickListener
            }

            val text = inputText.toString()
            //basically when argument in nav file isn't nullable i cant send arguments
            //if its nullable app launches but it doesnt send anythong
            val action = fragmentOneDirections.actionFragmentOneToFragmentTwo(text)

            controller.navigate(action)


        }
    }

}