package com.example.fragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //doesn't work without this. had to seek solution on google.
        val navHostFragment =  supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment

        val bottomNavView = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        val controller = navHostFragment.navController

        val appBarConfig = AppBarConfiguration(setOf(
            R.id.fragmentOne,
            R.id.fragmentTwo,
            R.id.fragmentThree,
            R.id.fragmentFour
        ))

        setupActionBarWithNavController(controller,appBarConfig)
        bottomNavView.setupWithNavController(controller)



    }
}