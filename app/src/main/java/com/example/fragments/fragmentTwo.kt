package com.example.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.TextView


class fragmentTwo : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<TextView>(R.id.recieveinput).text = fragmentTwoArgs.fromBundle(requireArguments()).text.toString()
    }


}